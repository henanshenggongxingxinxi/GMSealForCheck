﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Extensions
{
    public class Define
    {
        private static string _LicDir;
        public static string Lic
        {
            get
            {
                if (string.IsNullOrEmpty(_LicDir))
                {
                    _LicDir = Path.Combine(AppContext.BaseDirectory, "Lic");
                    if (!Directory.Exists(_LicDir))
                    {
                        Directory.CreateDirectory(_LicDir);
                    }
                }
                return _LicDir;
            }
        }

        private static string _DbDir;
        public static string Db
        {
            get
            {
                if (string.IsNullOrEmpty(_DbDir))
                {
                    _DbDir = Path.Combine(AppContext.BaseDirectory, "Db");
                    if (!Directory.Exists(_DbDir))
                    {
                        Directory.CreateDirectory(_DbDir);
                    }
                }
                return _LicDir;
            }
        }

        /// <summary>
        /// 自检
        /// </summary>
        public static bool IsChecked = false;
    }
}
