﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SealManagement.Migrations
{
    public partial class UpdateProduceSign : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InfoLogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<int>(nullable: false),
                    UserName = table.Column<string>(maxLength: 50, nullable: false),
                    UserType = table.Column<int>(nullable: false),
                    ManageType = table.Column<string>(nullable: false),
                    CreateDate = table.Column<string>(maxLength: 30, nullable: false),
                    Remark = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfoLogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MenuItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserType = table.Column<int>(nullable: false),
                    MenuString = table.Column<string>(nullable: true),
                    MenuPath = table.Column<string>(nullable: true),
                    ParentId = table.Column<int>(nullable: false),
                    MenuItemId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MenuItems_MenuItems_MenuItemId",
                        column: x => x.MenuItemId,
                        principalTable: "MenuItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProduceSigns",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SealName = table.Column<string>(nullable: true),
                    SealType = table.Column<int>(nullable: false),
                    CreateTime = table.Column<string>(nullable: true),
                    StartTime = table.Column<string>(nullable: true),
                    EndTime = table.Column<string>(nullable: true),
                    MakerCertSN = table.Column<string>(nullable: true),
                    MakerCertBase64 = table.Column<string>(nullable: true),
                    UserCertSN = table.Column<string>(nullable: true),
                    UserCertBase64 = table.Column<string>(nullable: true),
                    SealBase64 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProduceSigns", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SignMakers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserName = table.Column<string>(maxLength: 50, nullable: false),
                    ArrayNo = table.Column<string>(maxLength: 50, nullable: false),
                    Certificator = table.Column<string>(nullable: false),
                    CreateDate = table.Column<string>(maxLength: 30, nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    IsDelete = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SignMakers", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MenuItems_MenuItemId",
                table: "MenuItems",
                column: "MenuItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InfoLogs");

            migrationBuilder.DropTable(
                name: "MenuItems");

            migrationBuilder.DropTable(
                name: "ProduceSigns");

            migrationBuilder.DropTable(
                name: "SignMakers");
        }
    }
}
