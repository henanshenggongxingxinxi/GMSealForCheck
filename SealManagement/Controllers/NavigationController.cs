﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using SealManagement.Common;
using SealManagement.Models;
using SealManagement.SqlData;

namespace SealManagement.Controllers
{
    public class NavigationController : Controller
    {
        private SignContext _context;
        private IMemoryCache _cache;
        public NavigationController(SignContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }
        public IActionResult Index()
        {
            #region 登录
            if (HttpContext.User.Claims.Count() == 0)
            {
                return Json(new { state = resultType.Error });
            }
            #endregion
            var resListMenus = new List<MenuItem>();
            var utypeStr = HttpContext.User.FindFirst(ClaimTypes.Role).Value;
            if (utypeStr == userType.SignMaker.ToString())
            {
                if (_cache.Get("MenuItems_SignMaker") == null)
                {
                    resListMenus = _context.MenuItems.Where(p => p.UserType == (int)userType.SignMaker && p.ParentId == 0).ToList();
                    foreach (var item in resListMenus)
                    {
                        var ChildNodes = _context.MenuItems.Where(p => p.ParentId == item.Id).ToList();
                        item.ChildNodes = ChildNodes;
                    }
                    _cache.Set("MenuItems_SignMaker", resListMenus);
                }
                else
                {
                    resListMenus = _cache.Get("MenuItems_SignMaker") as List<MenuItem>;
                }
            }
            else
            {
                if (_cache.Get("MenuItems_User") == null)
                {
                    resListMenus = _context.MenuItems.Where(p => p.UserType == (int)userType.User && p.ParentId == 0).ToList();
                    foreach (var item in resListMenus)
                    {
                        var ChildNodes = _context.MenuItems.Where(p => p.ParentId == item.Id).ToList();
                        item.ChildNodes = ChildNodes;
                    }
                    _cache.Set("MenuItems_User", resListMenus);
                }
                else
                {
                    resListMenus = _cache.Get("MenuItems_User") as List<MenuItem>;
                }
            }
            //if (_cache.Get("MenuItems") == null)
            //{
            //    var utypeStr = HttpContext.User.FindFirst(ClaimTypes.Role).Value;
            //    var utype = 0;
            //    if (utypeStr == userType.SignMaker.ToString())
            //        utype = 1;
            //    var listMenu = _context.MenuItems.Where(p => p.UserType == utype && p.ParentId == 0).ToList();
            //    foreach (var item in listMenu)
            //    {
            //        var ChildNodes = _context.MenuItems.Where(p => p.ParentId == item.Id).ToList();
            //        item.ChildNodes = ChildNodes;
            //    }
            //    _cache.Set("MenuItems", listMenu);
            //    resListMenus = listMenu;
            //}
            //else
            //{
            //    resListMenus = _cache.Get("MenuItems") as List<MenuItem>;
            //}
            return Json(new { state = resultType.Success, data = resListMenus });
        }
    }
}