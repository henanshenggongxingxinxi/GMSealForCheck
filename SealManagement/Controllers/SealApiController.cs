﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NLog;
using SealManagement.Common;
using SealManagement.Extensions;
using SealManagement.SqlData;

namespace SealManagement.Controllers
{
    public class SealApiController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private SignContext _context;

        public SealApiController(SignContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> GetSealState(string arrayNo)
        {
            var produceSign = _context.ProduceSigns.FirstOrDefault(s => s.UserCertSN.Equals(arrayNo, StringComparison.OrdinalIgnoreCase));
            if (produceSign == null)
            {
                logger.Info($"获取印章状态：操作失败|不存在该印章信息");
                return Json(new { state = resultType.Error, message = "不存在该印章信息", errorCode = -1 });
            }
            logger.Info($"获取印章状态：操作成功|获取印章状态成功");
            return Json(new { state = resultType.Success, message = "获取印章状态成功", data = produceSign.IsAudit, errorCode = 0 });
        }
    }
}