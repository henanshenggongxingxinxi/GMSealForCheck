﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SealManagement.Models;

namespace SealManagement.SqlData
{
    /// <summary>
    /// 创建上下文类
    /// </summary>
    public class SignContext : DbContext
    {
        public SignContext(DbContextOptions<SignContext> options) : base(options)
        { }

        /// <summary>
        /// 用户——管理员
        /// </summary>
        public DbSet<User> Users { get; set; }
        /// <summary>
        /// 用户——制章人
        /// </summary>
        public DbSet<SignMaker> SignMakers { get; set; }
        /// <summary>
        /// 日志记录
        /// </summary>
        public DbSet<InfoLog> InfoLogs { get; set; }
        /// <summary>
        /// 制章
        /// </summary>
        public DbSet<ProduceSign> ProduceSigns { get; set; }
        /// <summary>
        /// 菜单项
        /// </summary>
        public DbSet<MenuItem> MenuItems { get; set; }
    }
}
