﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SealManagement.Common;
using SealManagement.Extensions;
using SealManagement.Models;

namespace SealManagement.SqlData
{
    public static class DbInitializer
    {
        public static void Initialize(SignContext context, IOptions<AppSettings> options)
        {
            try
            {
                //context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }
            catch
            {
                Console.WriteLine("failed to construct database!");
            }

            if (!context.Users.Any())//初始化用户表
            {
                var cerPath = Path.Combine(Define.Lic, options.Value.AdminCertPath);
                byte[] certBytes = null;
                using (FileStream fsRead = new FileStream(cerPath, FileMode.Open))
                {
                    int fsLen = (int)fsRead.Length;
                    certBytes = new byte[fsLen];
                    int r = fsRead.Read(certBytes, 0, certBytes.Length);
                }
                X509Certificate2 cert = new X509Certificate2(certBytes);
                var serialNum = cert.SerialNumber;
                var certBase64 = Convert.ToBase64String(cert.RawData);

                var users = new User[]
                {
                    new User{
                        ArrayNo ="42890EB6353E0B711A610CE9CF0D708B",
                        Certificator ="MIIDDzCCArOgAwIBAgIQQokOtjU+C3EaYQzpzw1wizAMBggqgRzPVQGDdQUAMGYxCzAJBgNVBAYTAkNOMQ4wDAYDVQQIDAVIZU5hbjESMBAGA1UEBwwJWmhlbmdaaG91MSQwIgYDVQQKDBtIZU5hbiBDZXJ0aWZpY2F0ZSBBdXRob3JpdHkxDTALBgNVBAMMBEhOQ0EwHhcNMTkwNzI2MDMxMTU1WhcNMjAwNzI1MTYwMDAwWjCBlTELMAkGA1UEBhMCQ04xEjAQBgNVBAgMCeays+WNl+ecgTESMBAGA1UEBwwJ6YOR5bee5biCMS0wKwYDVQQKDCTljY7mtYvnlLXlrZDorqTor4HmnInpmZDotKPku7vlhazlj7gxGzAZBgNVBAsMEueUteWtkOetvueroOmAgeajgDESMBAGA1UEAwwJ566h55CG5ZGYMFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEnT6tw/5exBhGYccfeE99miv1ripDr59mTjaQqBssiN1+HjEliSnVdneiElvDVN+9MDti6zySNigLLrOkK9GdfqOCAQ8wggELMIG7BgNVHR8EgbMwgbAwNqA0oDKkMDAuMQswCQYDVQQGEwJDTjEMMAoGA1UECwwDQ1JMMREwDwYDVQQDDAhjcmwxMTU1NjB2oHSgcoZwbGRhcDovLzIxOC4yOC4xNi4xMDE6Mzg3L0NOPWNybDExNTU2LE9VPUNSTCxDPUNOP2NlcnRpZmljYXRlUmV2b2NhdGlvbkxpc3Q/YmFzZT9vYmplY3RjbGFzcz1jUkxEaXN0cmlidXRpb25Qb2ludDAfBgNVHSMEGDAWgBSF6SJCjmIZ2wSWAxIx3BGQ3w7GtTAdBgNVHQ4EFgQUOy5b7ItLRSpaw6R5j5S143lOaSMwCwYDVR0PBAQDAgbAMAwGCCqBHM9VAYN1BQADSAAwRQIgLfQklAcDgryLOQKWfrkzZO4Cvt6lwKWwhEEZCwmhF7YCIQDf4a1mjxMLoe1kDYSgLjrseikiByd8Qz2OGqoyVFwaag==",
                        CreateDate =DateTime.Now.ToString(),
                        IsDelete =0,
                        UserName ="admin",
                        Remark ="123456"
                    }
                };
                context.Users.AddRange(users);
            }
            if (!context.MenuItems.Any())//初始化菜单表
            {
                var menus = new MenuItem[]
                {
                    new MenuItem{ MenuPath="#", MenuString="管理员管理", ParentId=0, UserType=(int)userType.User},//1
                    new MenuItem{ MenuPath="/User/index", MenuString="查看管理员", ParentId=1, UserType=(int)userType.User},//2
                    new MenuItem{ MenuPath="/User/create", MenuString="添加管理员", ParentId=1, UserType=(int)userType.User},//3
                    new MenuItem{ MenuPath="#", MenuString="制章人管理", ParentId=0, UserType=(int)userType.User},//4 
                    new MenuItem{ MenuPath="/signmaker/index", MenuString="查看制章人", ParentId=4, UserType=(int)userType.User},//5
                    new MenuItem{ MenuPath="/signmaker/create", MenuString="添加制章人", ParentId=4, UserType=(int)userType.User},//6
                    new MenuItem{ MenuPath="#", MenuString="印章管理", ParentId=0, UserType=(int)userType.SignMaker},//7
                    new MenuItem{ MenuPath="/producesign/create", MenuString="制章", ParentId=7, UserType=(int)userType.SignMaker},
                    new MenuItem{ MenuPath="/producesign/verify", MenuString="验章", ParentId=7, UserType=(int)userType.SignMaker},
                    new MenuItem{ MenuPath="/producesign/restore", MenuString="恢复印章", ParentId=7, UserType=(int)userType.SignMaker},
                    new MenuItem{ MenuPath="#", MenuString="日志管理", ParentId=0, UserType=(int)userType.SignMaker},//11
                    new MenuItem{ MenuPath="/infolog/index", MenuString="查看日志", ParentId=11, UserType=(int)userType.SignMaker},
                    new MenuItem{ MenuPath="#", MenuString="日志管理", ParentId=0, UserType=(int)userType.User},//13
                    new MenuItem{ MenuPath="/infolog/index", MenuString="查看日志", ParentId=13, UserType=(int)userType.User},
                    new MenuItem{ MenuPath="/infolog/backup", MenuString="备份恢复全库", ParentId=13, UserType=(int)userType.User},
                    //new MenuItem{ MenuPath="#", MenuString="印章管理", ParentId=0, UserType=(int)userType.User},
                    //new MenuItem{ MenuPath="/producesign/index", MenuString="印章列表", ParentId=16, UserType=(int)userType.User},
                };
                context.MenuItems.AddRange(menus);
            }
            context.SaveChangesAsync();
        }
    }
}
