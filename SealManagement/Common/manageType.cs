﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Common
{
    /// <summary>
    /// 操作类型
    /// </summary>
    public enum manageType
    {
        Login,
        Logout,
        AddUser,
        EditUser,
        DelUser,
        AddSignMaker,
        EditSignMaker,
        DelSignMaker,
        MakeSign,
        VerifySign,
        RecoverSign,
        BackUp,
        FileRecover,
        DownLoadFile,
        AuditSeal,
        FrozenSeal
    }
    /// <summary>
    /// 用户类型
    /// </summary>
    public enum userType
    {
        User = 0,
        SignMaker = 1
    }
    /// <summary>
    /// 结果类型
    /// </summary>
    public enum resultType
    {
        Success = 200,
        Error = 404,
    }
}
