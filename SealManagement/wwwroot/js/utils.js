function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
} 

//获取证书列表,显示到选择框里
function EnumKey(funcBack, errorCallBack) {
	//证明信息
	var socket = new WebSocket("ws://127.0.0.1:2012/CertInfo");
	try {
		socket.onerror = errorCallBack;
		socket.onopen = function(msg) {
			socket.send("abc");
		};
		//onmessage拿到服务器返回的结果
		socket.onmessage = function(e) {
			console.log(e.data, "证书列表")
			if (isNumber(e.data)) {
				funcBack("");
			} else {
				//alert(e.data);
				funcBack(e.data);
			}
		};
	} catch (ex) {
		funcBack("");
	}
}
//3.计算签名
function SignByHash(strJson, funcBack) {

	//alert(strJson);
	var socket = new WebSocket("ws://127.0.0.1:2012/SignDataEx");
	try {

		socket.onopen = function(msg) {
			//alert("连接成功！");
			socket.send(strJson);
		};
		socket.onmessage = function(e) {
			console.log(e.data, '计算签名')
			funcBack(e.data);
			socket.close();
		};
	} catch (ex) {
		funcBack("");
		socket.close();
	}

}
//1.签名挑战
function VerifyPin(strJson, funcBack) {
	//alert(strJson);
	var socket = new WebSocket("ws://127.0.0.1:2012/VerifyPin");
	try {

		socket.onopen = function(msg) {
			//alert("连接成功！");
			socket.send(strJson);
		};
		socket.onmessage = function(e) {
			console.log(e.data, '签名挑战')
			funcBack(e.data);
			socket.close();
		};
	} catch (ex) {
		funcBack("");
		socket.close();
	}

}


//获取证书和印章
function GetCertAndSeal(certSN, funcBack) {
	//alert(certSN);
	var socket = new WebSocket("ws://127.0.0.1:2012/SealAndCertBase64");
	try {

		socket.onopen = function(msg) {

			//alert("连接成功！");
			socket.send(certSN);
		};
		socket.onmessage = function(e) {
			console.log(e.data,"获取证书和印章")
			funcBack(e.data);
			socket.close();
		};
	} catch (ex) {
		funcBack("");
		socket.close();
	}

}

function GetPinCode(certSN, funcBack) {
	//alert(certSN);
	var socket = new WebSocket("ws://127.0.0.1:2012/PromptPin");
	try {

		socket.onopen = function(msg) {

			//alert("连接成功！");
			socket.send(certSN);
		};
		socket.onmessage = function(e) {
			funcBack(e.data);
			socket.close();
		};
	} catch (ex) {
		funcBack("");
		socket.close();
	}

}



//2.计算哈希值
function ComputeHash(strJson, funcBack) {
	//alert(certSN);
	var socket = new WebSocket("ws://127.0.0.1:2012/Hash");
	try {

		socket.onopen = function(msg) {
			//alert("连接成功！");
			socket.send(strJson);
		};
		socket.onmessage = function(e) {
			console.log(e.data, "计算哈希")
			funcBack(e.data);
			socket.close();
		};
	} catch (ex) {
		funcBack("");
		socket.close();
	}

}

function GetAuth(strJson, funcBack) {

	//alert(strJson);
	var socket = new WebSocket("ws://127.0.0.1:2012/Auth");
	try {

		socket.onopen = function(msg) {
			//alert("连接成功！");
			socket.send(strJson);
		};
		socket.onmessage = function(e) {
			funcBack(e.data);
			socket.close();
		};
	} catch (ex) {
		funcBack("");
		socket.close();
	}

}

function GetSerialId(certSN, funcBack) {
	var socket = new WebSocket("ws://127.0.0.1:2012/SerialId");
	try {

		socket.onopen = function(msg) {
			socket.send(certSN);
		};
		socket.onmessage = function(e) {
			funcBack(e.data);
			socket.close();
		};
	} catch (ex) {
		funcBack("");
		socket.close();
	}

}

function GetProducts(certSN, funcBack) {
	var socket = new WebSocket("ws://127.0.0.1:2012/Products");
	try {

		socket.onopen = function(msg) {
			socket.send(certSN);
		};
		socket.onmessage = function(e) {
			funcBack(e.data);
			socket.close();
		};
	} catch (ex) {
		funcBack("");
		socket.close();
	}

	function GetCertAndSeal(certSN, funcBack) {
		//alert(certSN);
		var socket = new WebSocket("ws://127.0.0.1:2012/SealAndCertBase64");
		try {

			socket.onopen = function(msg) {

				//alert("连接成功！");
				socket.send(certSN);
			};
			socket.onmessage = function(e) {
				funcBack(e.data);
				socket.close();
			};
		} catch (ex) {
			funcBack("");
			socket.close();
		}

	}
}
//外部加密
function ExternalEncrypt(Json, funcBack) {
	console.log(Json)
	var socket = new WebSocket("ws://127.0.0.1:2012/CustomizeEnvelope");
	try {

		socket.onopen = function(msg) {
			socket.send(Json);
		};
		socket.onmessage = function(e) {
			console.log(e.data, "加密");
			funcBack(e.data);
			socket.close();
		};
	} catch (ex) {
		funcBack("");
		socket.close();
	}

}

function EncryptCertList(back) {
	var socket = new WebSocket("ws://127.0.0.1:2012/EncryptCertList");
	try {

		socket.onopen = function(msg) {
			socket.send("1");
		};
		socket.onmessage = function(e) {
			back(e.data);
			socket.close();
		};
	} catch (ex) {
		funcBack("");
		socket.close();
	}
}

function isNumber(val) {
	var regPos = /^\d+(\.\d+)?$/; //非负浮点数
	var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
	if (regPos.test(val) || regNeg.test(val)) {
		return true;
	} else {
		return false;
	}
}
//显示信息
function DisplayMessage(message) {
	var mydate = new Date();
	$("#infoDiv").append("<dt>" +
		mydate.Format("hh:mm:ss") +
		"</dt><dd>" +
		message +
		"</dd>");
}
//Base64编码
function Base64() {
	// private property
	_keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

	// public method for encoding
	this.encode = function(input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;
		input = _utf8_encode(input);
		while (i < input.length) {
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);
			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;
			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}
			output = output +
				_keyStr.charAt(enc1) +
				_keyStr.charAt(enc2) +
				_keyStr.charAt(enc3) +
				_keyStr.charAt(enc4);
		}
		return output;
	};

	// public method for decoding
	this.decode = function(input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;
		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
		while (i < input.length) {
			enc1 = _keyStr.indexOf(input.charAt(i++));
			enc2 = _keyStr.indexOf(input.charAt(i++));
			enc3 = _keyStr.indexOf(input.charAt(i++));
			enc4 = _keyStr.indexOf(input.charAt(i++));
			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;
			output = output + String.fromCharCode(chr1);
			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}
		}
		output = _utf8_decode(output);
		return output;
	};

	// private method for UTF-8 encoding
	_utf8_encode = function(string) {
		string = string.replace(/\r\n/g, "\n");
		var utftext = "";
		for (var n = 0; n < string.length; n++) {
			var c = string.charCodeAt(n);
			if (c < 128) {
				utftext += String.fromCharCode(c);
			} else if ((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			} else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}
		return utftext;
	};

	// private method for UTF-8 decoding
	_utf8_decode = function(utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
		while (i < utftext.length) {
			c = utftext.charCodeAt(i);
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			} else if ((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i + 1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			} else {
				c2 = utftext.charCodeAt(i + 1);
				c3 = utftext.charCodeAt(i + 2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
		}
		return string;
	};
}
