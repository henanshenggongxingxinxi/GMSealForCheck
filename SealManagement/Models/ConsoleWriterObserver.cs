﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    public class ConsoleWriterObserver
    {
        public void OnNext(SqliteBackupEvent value)
        {
            //Console.WriteLine("{0} - {1} - {2} - {3}", value.Pages, value.PageCount, value.Remaining, value.Retry);
        }

        public void OnError(Exception error)
        {
            //Console.WriteLine(error.Message);
        }

        public void OnCompleted()
        {
            //Console.WriteLine("Complete");
        }
    }
}
