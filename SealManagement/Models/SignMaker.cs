﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    public class SignMaker
    {
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// 登录名
        /// </summary>
        [Required]
        [StringLength(50)]
        [DisplayName("用户名")]
        public string UserName { get; set; }
        /// <summary>
        /// 序列号
        /// </summary>
        [Required]
        [StringLength(50)]
        [DisplayName("序列号")]
        public string ArrayNo { get; set; }
        /// <summary>
        /// 证书base64
        /// </summary>
        [Required]
        [DisplayName("证书")]
        public string Certificator { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        [Required]
        [StringLength(30)]
        [DisplayName("创建日期")]
        public string CreateDate { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("备注")]
        public string Remark { get; set; }
        /// <summary>
        /// 删除标识
        /// </summary>
        [DisplayName("删除标识")]
        public int IsDelete { get; set; }
    }
}
