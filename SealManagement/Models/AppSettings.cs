﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    public class AppSettings
    {
        /// <summary>
        /// .gx文件位置
        /// </summary>
        public string GXPosition { get; set; }
        /// <summary>
        /// 身份验证服务端返回的证书base64
        /// </summary>
        public string CertBase64 { get; set; }
        /// <summary>
        /// 父级证书
        /// </summary>
        public string ParentCertPosition { get; set; }
        /// <summary>
        /// 国家政务信任支撑系统地址
        /// </summary>
        public string DependenSupportUrl { get; set; }
        /// <summary>
        /// 管理员登录证书路径
        /// </summary>
        public string AdminCertPath { get; set; }
    }
}
