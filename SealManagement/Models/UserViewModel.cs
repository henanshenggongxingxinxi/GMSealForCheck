﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    public class UserViewModel
    {
        public User User { get; set; }
        public string ValidateCode { get; set; }
    }
}
