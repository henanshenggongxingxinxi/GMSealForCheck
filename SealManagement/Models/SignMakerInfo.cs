﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    public class SignMakerInfo
    {
        /// <summary>
        /// 证书颁发者
        /// </summary>
        [DisplayName("证书颁发者")]
        public string Issuer { get; set; }
        /// <summary>
        /// 证书使用者
        /// </summary>
        [DisplayName("证书使用者")]
        public string Subject { get; set; }
        /// <summary>
        /// 证书过期日期
        /// </summary>
        [DisplayName("证书过期日期")]
        public string NotAfter { get; set; }
        /// <summary>
        /// 证书开始日期
        /// </summary>
        [DisplayName("证书开始日期")]
        public string NotBefore { get; set; }

        public SignMaker SignMaker { get; set; }
    }
}
