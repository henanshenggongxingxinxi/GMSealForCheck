﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    public class ProduceSignViewModel
    {
        public ProduceSign ProduceSign { get; set; }
        public string ValidateCode { get; set; }
        public string UserCN { get; set; }
    }
}
