﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    public class LoginModel
    {
        public string ValidateCode { get; set; }
        public string CertID { get; set; }
        public string SignCert { get; set; }
    }
}
