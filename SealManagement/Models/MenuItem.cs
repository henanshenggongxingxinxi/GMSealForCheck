﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    public class MenuItem
    {
        [Key]
        public int Id { get; set; }
        [DisplayName("所属权限人类别")]
        public int UserType { get; set; }
        [DisplayName("菜单名称")]
        public string MenuString { get; set; }
        [DisplayName("菜单路径")]
        public string MenuPath { get; set; }
        public int ParentId { get; set; }

        public List<MenuItem> ChildNodes { get; set; }
    }
}
