﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    public class FileModel
    {
        public string FileContent { get; set; }
        public string FileName { get; set; }
        public string FileSuffix { get; set; }
        public string ValidateCode { get; set; }
    }
}
