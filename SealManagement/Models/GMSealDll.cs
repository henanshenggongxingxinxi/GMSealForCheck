﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    public class GMSealDll
    {
        public static object Lock = new object();

        /// <summary>
        /// 自检函数
        /// </summary>
        /// <returns></returns>
        [DllImport("libloadswsds-x64.so", EntryPoint = "SGD_SelfCheck", CallingConvention = CallingConvention.StdCall)]
        public static extern int SGD_SelfCheck();

        /// <summary>
        /// 板卡解密
        /// </summary>
        /// <param name="ciphertext"></param>
        /// <param name="ciphertextlen"></param>
        /// <param name="plaintext"></param>
        /// <param name="plaintextlen"></param>
        /// <returns></returns>
        [DllImport("libloadswsds-x64.so", EntryPoint = "SGD_ExtAndDateDecrypt", CallingConvention = CallingConvention.StdCall)]
        public static extern int SGD_ExtAndDateDecrypt(byte[] ciphertext, int ciphertextlen, byte[] plaintext, ref int plaintextlen);
    }
}
