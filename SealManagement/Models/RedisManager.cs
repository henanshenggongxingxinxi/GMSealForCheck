﻿using CSRedis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace SealManagement.Models
{
    public class RedisManager : IDisposable
    {
        public RedisManager(string ipAndPort)
        {
            var csredis = new CSRedis.CSRedisClient(ipAndPort + ",poolsize=100,preheat=true,writeBuffer=102400,tryit=3");
            RedisHelper.Initialization(csredis);
        }

        public RedisManager() : this("127.0.0.1:6379")
        {
        }

        private readonly object locker = new object();
        public CSRedisClient Instance
        {
            get { return RedisHelper.Instance; }
        }

        public void Dispose()
        {
            lock (locker)
            {
                RedisHelper.Instance.Dispose();
            }
        }
    }
}
