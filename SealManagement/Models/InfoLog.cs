﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    public class InfoLog
    {
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// 操作人Id
        /// </summary>
        [Required]
        [DisplayName("操作人Id")]
        public int UserId { get; set; }
        /// <summary>
        /// 操作人
        /// </summary>
        [Required]
        [StringLength(50)]
        [DisplayName("操作人")]
        public string UserName { get; set; }
        /// <summary>
        /// 操作人类别
        /// </summary>
        [Required]
        [DisplayName("操作人类别")]
        public int UserType { get; set; }
        /// <summary>
        /// 操作类别
        /// </summary>
        [Required]
        [DisplayName("操作类别")]
        public string ManageType { get; set; }
        /// <summary>
        /// 操作日期
        /// </summary>
        [Required]
        [StringLength(30)]
        [DisplayName("操作日期")]
        public string CreateDate { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [DisplayName("备注")]
        public string Remark { get; set; }
    }
}
