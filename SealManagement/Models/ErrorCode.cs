﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    [Flags]
    public enum ErrorCode
    {
        /*常量定义*/
        SGD_TRUE = 0x00000001,
        SGD_FALSE = 0x00000000,

        /*算法标识*/
        SGD_SM1_ECB = 0x00000101,
        SGD_SM1_CBC = 0x00000102,
        SGD_SM1_CFB = 0x00000104,
        SGD_SM1_OFB = 0x00000108,
        SGD_SM1_MAC = 0x00000110,

        SGD_SSF33_ECB = 0x00000201,
        SGD_SSF33_CBC = 0x00000202,
        SGD_SSF33_CFB = 0x00000204,
        SGD_SSF33_OFB = 0x00000208,
        SGD_SSF33_MAC = 0x00000210,

        SGD_AES_ECB = 0x00000401,
        SGD_AES_CBC = 0x00000402,
        SGD_AES_CFB = 0x00000404,
        SGD_AES_OFB = 0x00000408,
        SGD_AES_MAC = 0x00000410,

        SGD_3DES_ECB = 0x00000801,
        SGD_3DES_CBC = 0x00000802,
        SGD_3DES_CFB = 0x00000804,
        SGD_3DES_OFB = 0x00000808,
        SGD_3DES_MAC = 0x00000810,

        SGD_SMS4_ECB = 0x00002001,
        SGD_SMS4_CBC = 0x00002002,
        SGD_SMS4_CFB = 0x00002004,
        SGD_SMS4_OFB = 0x00002008,
        SGD_SMS4_MAC = 0x00002010,

        SGD_DES_ECB = 0x00004001,
        SGD_DES_CBC = 0x00004002,
        SGD_DES_CFB = 0x00004004,
        SGD_DES_OFB = 0x00004008,
        SGD_DES_MAC = 0x00004010,

        SGD_RSA = 0x00010000,
        SGD_RSA_SIGN = 0x0001010,
        SGD_RSA_ENC = 0x00010200,
        SGD_SM2_1 = 0x00020100,
        SGD_SM2_2 = 0x00020200,
        SGD_SM2_3 = 0x00020400,

        SGD_SM3 = 0x00000001,
        SGD_SHA1 = 0x00000002,
        SGD_SHA256 = 0x00000004,
        SGD_SHA512 = 0x00000008,
        SGD_SHA384 = 0x00000010,
        SGD_SHA224 = 0x00000020,
        SGD_MD5 = 0x00000080,


        /*标准错误码定义*/
        [Description("成功")]
        SDR_OK = 0x0,                          /*成功*/
        SDR_BASE = 0x01000000,
        [Description("未知错误")]
        SDR_UNKNOWERR = (SDR_BASE + 0x00000001),       /*未知错误*/
        [Description("不支持")]
        SDR_NOTSUPPORT = (SDR_BASE + 0x00000002),      /*不支持*/
        [Description("通信错误")]
        SDR_COMMFAIL = (SDR_BASE + 0x00000003), /*通信错误*/
        [Description("硬件错误")]
        SDR_HARDFAIL = (SDR_BASE + 0x00000004),/*硬件错误*/
        [Description("打开设备错误")]
        SDR_OPENDEVICE = (SDR_BASE + 0x00000005), /*打开设备错误*/
        [Description("打开会话句柄错误")]
        SDR_OPENSESSION = (SDR_BASE + 0x00000006), /*打开会话句柄错误*/
        [Description("权限不满足")]
        SDR_PARDENY = (SDR_BASE + 0x00000007), /*权限不满足*/
        [Description("密钥不存在")]
        SDR_KEYNOTEXIST = (SDR_BASE + 0x00000008),    /*密钥不存在*/
        [Description("不支持的算法")]
        SDR_ALGNOTSUPPORT = (SDR_BASE + 0x00000009), /*不支持的算法*/
        [Description("不支持的算法模式")]
        SDR_ALGMODNOTSUPPORT = (SDR_BASE + 0x0000000A),   /*不支持的算法模式*/
        [Description("公钥运算错误")]
        SDR_PKOPERR = (SDR_BASE + 0x0000000B), /*公钥运算错误*/
        [Description("私钥运算错误")]
        SDR_SKOPERR = (SDR_BASE + 0x0000000C),  /*私钥运算错误*/
        [Description("签名错误")]
        SDR_SIGNERR = (SDR_BASE + 0x0000000D), /*签名错误*/
        [Description("验证错误")]
        SDR_VERIFYERR = (SDR_BASE + 0x0000000E),  /*验证错误*/
        [Description("对称运算错误")]
        SDR_SYMOPERR = (SDR_BASE + 0x0000000F),    /*对称运算错误*/
        [Description("步骤错误")]
        SDR_STEPERR = (SDR_BASE + 0x00000010),  /*步骤错误*/
        [Description("文件大小错误或输入数据长度非法")]
        SDR_FILESIZEERR = (SDR_BASE + 0x00000011),    /*文件大小错误或输入数据长度非法*/
        [Description("文件不存在")]
        SDR_FILENOEXIST = (SDR_BASE + 0x00000012),   /*文件不存在*/
        [Description("文件操作偏移量错误")]
        SDR_FILEOFSERR = (SDR_BASE + 0x00000013),  /*文件操作偏移量错误*/
        [Description("密钥类型错误")]
        SDR_KEYTYPEERR = (SDR_BASE + 0x00000014),  /*密钥类型错误*/
        [Description("密钥错误")]
        SDR_KEYERR = (SDR_BASE + 0x00000015), /*密钥错误*/

        /*============================================================*/
        /*扩展错误码*/
        [Description("自定义错误码基础值")]
        SWR_BASE = (SDR_BASE + 0x00010000),/*自定义错误码基础值*/
        [Description("无效的用户名")]
        SWR_INVALID_USER = (SWR_BASE + 0x00000001), /*无效的用户名*/
        [Description("无效的授权码")]
        SWR_INVALID_AUTHENCODE = (SWR_BASE + 0x00000002),   /*无效的授权码*/
        [Description("不支持的协议版本")]
        SWR_PROTOCOL_VER_ERR = (SWR_BASE + 0x00000003), /*不支持的协议版本*/
        [Description("错误的命令字")]
        SWR_INVALID_COMMAND = (SWR_BASE + 0x00000004),/*错误的命令字*/
        [Description("参数错误或错误的数据包格式")]
        SWR_INVALID_PARAMETERS = (SWR_BASE + 0x00000005),   /*参数错误或错误的数据包格式*/
        [Description("已存在同名文件")]
        SWR_FILE_ALREADY_EXIST = (SWR_BASE + 0x00000006),   /*已存在同名文件*/
        [Description("多卡同步错误")]
        SWR_SYNCH_ERR = (SWR_BASE + 0x00000007),    /*多卡同步错误*/
        [Description("多卡同步后登录错误")]
        SWR_SYNCH_LOGIN_ERR = (SWR_BASE + 0x00000008),  /*多卡同步后登录错误*/
        [Description("超时错误")]
        SWR_SOCKET_TIMEOUT = (SWR_BASE + 0x00000100),   /*超时错误*/
        [Description("连接服务器错误")]
        SWR_CONNECT_ERR = (SWR_BASE + 0x00000101),  /*连接服务器错误*/
        [Description("设置Socket参数错误")]
        SWR_SET_SOCKOPT_ERR = (SWR_BASE + 0x00000102),  /*设置Socket参数错误*/
        [Description("发送LOGINRequest错误")]
        SWR_SOCKET_SEND_ERR = (SWR_BASE + 0x00000104),  /*发送LOGINRequest错误*/
        [Description("发送LOGINRequest错误")]
        SWR_SOCKET_RECV_ERR = (SWR_BASE + 0x00000105),  /*发送LOGINRequest错误*/
        [Description("发送LOGINRequest错误")]
        SWR_SOCKET_RECV_0 = (SWR_BASE + 0x00000106),    /*发送LOGINRequest错误*/
        [Description("超时错误")]
        SWR_SEM_TIMEOUT = (SWR_BASE + 0x00000200),  /*超时错误*/
        [Description("没有可用的加密机")]
        SWR_NO_AVAILABLE_HSM = (SWR_BASE + 0x00000201), /*没有可用的加密机*/
        [Description("加密机内没有可用的加密模块")]
        SWR_NO_AVAILABLE_CSM = (SWR_BASE + 0x00000202), /*加密机内没有可用的加密模块*/
        [Description("配置文件错误")]
        SWR_CONFIG_ERR = (SWR_BASE + 0x00000301),   /*配置文件错误*/

        /*============================================================*/
        /*密码卡错误码*/
        [Description("密码卡错误码")]
        SWR_CARD_BASE = (SDR_BASE + 0x00020000),        /*密码卡错误码*/
        [Description("未知错误")]
        SWR_CARD_UNKNOWERR = (SWR_CARD_BASE + 0x00000001),  //未知错误
        [Description("不支持的接口调用")]
        SWR_CARD_NOTSUPPORT = (SWR_CARD_BASE + 0x00000002), //不支持的接口调用
        [Description("与设备通信失败")]
        SWR_CARD_COMMFAIL = (SWR_CARD_BASE + 0x00000003),   //与设备通信失败
        [Description("运算模块无响应")]
        SWR_CARD_HARDFAIL = (SWR_CARD_BASE + 0x00000004),   //运算模块无响应
        [Description("打开设备失败")]
        SWR_CARD_OPENDEVICE = (SWR_CARD_BASE + 0x00000005), //打开设备失败
        [Description("创建会话失败")]
        SWR_CARD_OPENSESSION = (SWR_CARD_BASE + 0x00000006),    //创建会话失败
        [Description("无私钥使用权限")]
        SWR_CARD_PARDENY = (SWR_CARD_BASE + 0x00000007),    //无私钥使用权限
        [Description("不存在的密钥调用")]
        SWR_CARD_KEYNOTEXIST = (SWR_CARD_BASE + 0x00000008),    //不存在的密钥调用
        [Description("不支持的算法调用")]
        SWR_CARD_ALGNOTSUPPORT = (SWR_CARD_BASE + 0x00000009),  //不支持的算法调用
        [Description("不支持的算法调用")]
        SWR_CARD_ALGMODNOTSUPPORT = (SWR_CARD_BASE + 0x00000010),   //不支持的算法调用
        [Description("公钥运算失败")]
        SWR_CARD_PKOPERR = (SWR_CARD_BASE + 0x00000011),    //公钥运算失败
        [Description("私钥运算失败")]
        SWR_CARD_SKOPERR = (SWR_CARD_BASE + 0x00000012),    //私钥运算失败
        [Description("签名运算失败")]
        SWR_CARD_SIGNERR = (SWR_CARD_BASE + 0x00000013),    //签名运算失败
        [Description("验证签名失败")]
        SWR_CARD_VERIFYERR = (SWR_CARD_BASE + 0x00000014),  //验证签名失败
        [Description("对称算法运算失败")]
        SWR_CARD_SYMOPERR = (SWR_CARD_BASE + 0x00000015),//对称算法运算失败
        [Description("多步运算步骤错误")]
        SWR_CARD_STEPERR = (SWR_CARD_BASE + 0x00000016),//多步运算步骤错误
        [Description("文件长度超出限制")]
        SWR_CARD_FILESIZEERR = (SWR_CARD_BASE + 0x00000017),    //文件长度超出限制
        [Description("指定的文件不存在")]
        SWR_CARD_FILENOEXIST = (SWR_CARD_BASE + 0x00000018),    //指定的文件不存在
        [Description("文件起始位置错误")]
        SWR_CARD_FILEOFSERR = (SWR_CARD_BASE + 0x00000019), //文件起始位置错误
        [Description("密钥类型错误")]
        SWR_CARD_KEYTYPEERR = (SWR_CARD_BASE + 0x00000020), //密钥类型错误
        [Description("密钥错误")]
        SWR_CARD_KEYERR = (SWR_CARD_BASE + 0x00000021), //密钥错误
        [Description("接收参数的缓存区太小")]
        SWR_CARD_BUFFER_TOO_SMALL = (SWR_CARD_BASE + 0x00000101),   //接收参数的缓存区太小
        [Description("数据没有按正确格式填充，或解密得到的脱密数据不符合填充格式")]
        SWR_CARD_DATA_PAD = (SWR_CARD_BASE + 0x00000102),   //数据没有按正确格式填充，或解密得到的脱密数据不符合填充格式
        [Description("明文或密文长度不符合相应的算法要求")]
        SWR_CARD_DATA_SIZE = (SWR_CARD_BASE + 0x00000103),  //明文或密文长度不符合相应的算法要求
        [Description("该错误表明没有为相应的算法调用初始化函数")]
        SWR_CARD_CRYPTO_NOT_INIT = (SWR_CARD_BASE + 0x00000104),	//该错误表明没有为相应的算法调用初始化函数

        //01/03/09版密码卡权限管理错误码
        [Description("管理权限不满足")]
        SWR_CARD_MANAGEMENT_DENY = (SWR_CARD_BASE + 0x00001001),    //管理权限不满足
        [Description("操作权限不满足")]
        SWR_CARD_OPERATION_DENY = (SWR_CARD_BASE + 0x00001002),//操作权限不满足
        [Description("当前设备状态不满足现有操作")]
        SWR_CARD_DEVICE_STATUS_ERR = (SWR_CARD_BASE + 0x00001003),  //当前设备状态不满足现有操作
        [Description("登录失败")]
        SWR_CARD_LOGIN_ERR = (SWR_CARD_BASE + 0x00001011),//登录失败
        [Description("用户ID数目/号码错误")]
        SWR_CARD_USERID_ERR = (SWR_CARD_BASE + 0x00001012),//用户ID数目/号码错误
        [Description("参数错误")]
        SWR_CARD_PARAMENT_ERR = (SWR_CARD_BASE + 0x00001013),   //参数错误

        //05/06版密码卡权限管理错误码
        [Description("管理权限不满足")]
        SWR_CARD_MANAGEMENT_DENY_05 = (SWR_CARD_BASE + 0x00000801), //管理权限不满足
        [Description("操作权限不满足")]
        SWR_CARD_OPERATION_DENY_05 = (SWR_CARD_BASE + 0x00000802),  //操作权限不满足
        [Description("当前设备状态不满足现有操作")]
        SWR_CARD_DEVICE_STATUS_ERR_05 = (SWR_CARD_BASE + 0x00000803),   //当前设备状态不满足现有操作
        [Description("登录失败")]
        SWR_CARD_LOGIN_ERR_05 = (SWR_CARD_BASE + 0x00000811),//登录失败
        [Description("用户ID数目/号码错误")]
        SWR_CARD_USERID_ERR_05 = (SWR_CARD_BASE + 0x00000812),  //用户ID数目/号码错误
        SWR_CARD_PARAMENT_ERR_05 = (SWR_CARD_BASE + 0x00000813),


        //自定义返回
        [Description("连接板卡驱动失败")]
        SWR_GX_ERR_01 = 0x1201,
        [Description("导出驱动函数失败")]
        SWR_GX_ERR_02 = 0x1202,
        [Description("解密数据有误")]
        SWR_GX_ERR_03 = 0x1203,
        [Description("自检数据有误")]
        SWR_GX_ERR_04 = 0x1204,
        [Description("自检数据对比失败")]
        SWR_GX_ERR_05 = 0x1205,
    }
}
