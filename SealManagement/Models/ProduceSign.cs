﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    public class ProduceSign
    {
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// 印章名称
        /// </summary>
        [DisplayName("印章名称")]
        public string SealName { get; set; }
        /// <summary>
        /// 印章类型  1单位印章 2个人印章
        /// </summary>
        [DisplayName("印章类型")]
        public int SealType { get; set; }
        /// <summary>
        /// 制章时间
        /// </summary>
        [DisplayName("制章时间")]
        public string CreateTime { get; set; }
        /// <summary>
        /// 生效开始时间
        /// </summary>
        [DisplayName("生效开始时间")]
        public string StartTime { get; set; }
        /// <summary>
        /// 生效结束时间
        /// </summary>
        [DisplayName("生效结束时间")]
        public string EndTime { get; set; }
        /// <summary>
        /// 制章人证书序列号
        /// </summary>
        [DisplayName("制章人证书序列号")]
        public string MakerCertSN { get; set; }
        /// <summary>
        /// 制章人证书Base64
        /// </summary>
        [DisplayName("制章人证书Base64")]
        public string MakerCertBase64 { get; set; }
        /// <summary>
        /// 用户证书序列号
        /// </summary>
        [DisplayName("用户证书序列号")]
        public string UserCertSN { get; set; }
        /// <summary>
        /// 用户证书Base64
        /// </summary>
        [DisplayName("用户证书Base64")]
        public string UserCertBase64 { get; set; }

        /// <summary>
        /// 印章数据
        /// </summary>
        [DisplayName("印章数据")]
        public string SealBase64 { get; set; }

        /// <summary>
        /// 是否审核
        /// </summary>
        [DisplayName("是否审核")]
        public int IsAudit { get; set; }

        /// <summary>
        /// 印章图片
        /// </summary>
        [DisplayName("印章图片")]
        public string SealImage { get; set; }
    }
}
