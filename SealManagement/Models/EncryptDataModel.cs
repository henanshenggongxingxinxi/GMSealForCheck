﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SealManagement.Models
{
    public class EncryptDataModel
    {
        public string CertId { get; set; }
        public string DataBase64 { get; set; }
    }
}
